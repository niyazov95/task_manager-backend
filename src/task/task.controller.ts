import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { TaskService } from './task.service';
import { TaskInterface } from './intrfaces/task.interface';
import { ApiImplicitParam, ApiUseTags } from '@nestjs/swagger';

@ApiUseTags('TASK')
@Controller('task')
export class TaskController {
  constructor(private readonly taskService: TaskService) {}
  @Get('getAllTask')
  async getAllTask(): Promise<TaskInterface[]> {
    return this.taskService.findAllTask();
  }

  @Get('getTaskById/:id')
  getTaskById(@Param('id') id): Promise<TaskInterface> {
    return this.taskService.findTaskById(id);
  }

  @Post('create')
  createTask(@Body() createTaskDto: CreateTaskDto): Promise<TaskInterface> {
    return this.taskService.create(createTaskDto);
  }

  @Delete('delete/:_id')
  @ApiImplicitParam({ name: '_id' })
  async deleteEvent(@Param('_id') _id: string): Promise<TaskInterface> {
    return this.taskService.delete(_id);
  }

  @Put('update/:id')
  updateTask(@Body() updateItemDto: CreateTaskDto, @Param('id') id): string {
    return `Update ${id}`;
  }
}

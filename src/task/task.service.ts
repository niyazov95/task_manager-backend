import { Injectable } from '@nestjs/common';
import { TaskInterface } from './intrfaces/task.interface';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class TaskService {
  constructor(@InjectModel('TASKs') private readonly taskModel: Model<TaskInterface>) {}
  private readonly task: TaskInterface[] = [
    {
      id: '353553',
      name: 'Task 1',
      description: 'Description',
      qty: 100,
    },
    {
      id: '231413',
      name: 'Task 2',
      description: 'Description',
      qty: 102,
    },
  ];

  async findAllTask(): Promise<TaskInterface[]> {
    return await this.taskModel.find();
  }
  async findTaskById(id: string): Promise<TaskInterface> {
    return await this.taskModel.findOne({ _id: id });
  }
  async create(task: TaskInterface): Promise<TaskInterface> {
    const newTask = new this.taskModel(task);
    return await newTask.save();
  }
  async delete(_id: string): Promise<TaskInterface> {
    return this.taskModel.findOneAndDelete(_id);
  }
}

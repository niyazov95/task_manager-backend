import { Document } from 'mongoose';

export interface TaskInterface extends Document {
  id?: string;
  name: string;
  description?: string;
  qty: number;
}

export class CreateTaskDto {
  readonly name: string;
  readonly description: string;
  readonly qty: number;
}

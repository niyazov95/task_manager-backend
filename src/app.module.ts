import { Module } from '@nestjs/common';
import { TaskModule } from './task/task.module';
import { MongooseModule } from '@nestjs/mongoose';
import config from './config/keys';

@Module({
  imports: [TaskModule, MongooseModule.forRoot(config.mongoURI)],
  controllers: [],
})
export class AppModule {}
